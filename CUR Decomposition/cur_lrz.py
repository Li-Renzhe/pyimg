# -*- coding: utf-8 -*-
"""
---------------------------------------------------
[File-Information:]
    -. Created on 2024/01/02
    -. Updated on 2024/01/18
    -. Developer: Akksay singh, Renzhe Li, Lei Li*
    -. Email: akksay@utexas.edu, 1135116956@qq.com
    -. Development Team: SUSTech Materials Department, Akksay singh, Renzhe Li, Lei Li*
---------------------------------------------------
[Description:]
--CUR decomposition technology based on global descriptors performs structural screening on the data set.
    -. This code has a good interface with Pyamff (https://gitlab.com/pyamff/pyamff);
    -. 'element' is the type of element in your system;
    -. 'path_data': Make sure the 'train.traj' and 'fingerprints files' are in this directory.
    -. 'want_img_list': The img number you want.
---------------------------------------------------
"""

element = 'Ge'
path_data ='/Users/renzhe/Desktop/code/CUR_Decomposition'
want_img_list = [3557, 3013, 2529, 2083, 1764, 1464, 1264, 1049, 877, 722, 639, 556, 432, 392]
# 1.Import all tools.
import io
import os
import re
import sys
import ase
import time
import torch
import pickle
import random
import sklearn
import numpy as np
import pandas as pd
from scipy import linalg
from ase.io.trajectory import Trajectory

# 2.Global Fingerprints for one image.
def cal_global_fingerprint(fp_path, el_=element):
    with open(fp_path, 'rb') as f:
        fp_all = pickle.load(f)
        fp_i = fp_all.allElement_fps[el_]
        time1_s = time.perf_counter()
        g_f = torch.sum(fp_i, axis=0) / len(fp_i)
    time1_e = time.perf_counter()
    time1 = time1_e - time1_s
    return g_f, time1


# 3.Global Fingerprints for all images.
def g_f_traj(path_all):
    time_all = 0
    gf_bank = []
    img_path = path_all + '/train.traj'
    img_all = Trajectory(img_path)
    img_num = len(img_all)
    for i in range(img_num):
        i_path = path_all + '/fingerprints/fps_{}.pckl'.format(i)
        res_i, t_ = cal_global_fingerprint(fp_path=i_path, el_='Ge')
        time_all += t_
        res_i_np = np.array(res_i)
        gf_bank.append(res_i_np)
        if i % 100 == 0:
            print('Finish cal_g_f {}'.format(i))
    df_gf = pd.DataFrame(gf_bank)
    df_gf.to_csv('./global_fps.csv', encoding='utf-8', index=True)
    print('Calculate Global_Fingerprints. The Run-Time:', time_all)


# 4.SVD --> score. V ：np.array
def score_from_kth_sing_vec(V, img_indx, _k=1):
    partial_score = 0.0
    for _i in range(_k):  # again, first singular vector is 0th index.
        score = (V[_i][img_indx]) ** 2
        partial_score += score
    return partial_score


# 5.INPUT matrix to this function has the column deleted and the original column.
def col_proj_sub(matrix_df, orig_data_col):
    matrix = np.array(matrix_df)
    selected_col = orig_data_col
    num_cols = np.shape(matrix)[1]
    norm_selected_col = np.linalg.norm(orig_data_col)
    for col in range(num_cols):
        dot_prod = np.dot(matrix[:, col], selected_col)
        proj = dot_prod / (norm_selected_col ** 2)
        matrix[:, col] -= selected_col * proj
    matrix_ = pd.DataFrame(matrix, columns=matrix_df.columns)
    return matrix_


# 6.Function to compute matrix scores of each column of original data matrix.
def calc_matrix_scores(matrix_df, k_=1):
    matrix_i = np.array(matrix_df)
    U, s, Vh = linalg.svd(matrix_i)  # Vh.shape = (7506, 7506)
    num_cols = np.shape(matrix_i)[1]
    scores_col = [score_from_kth_sing_vec(V=Vh, img_indx=col, _k=k_) for col in range(num_cols)]
    last_selected_fps_index_ = np.argmax(scores_col)
    return last_selected_fps_index_


# 7. Run-CUR_Decomposition.
def cur_decomposition(_path_):
    if not os.path.exists('./global_fps.csv'):
        g_f_traj(path_all=_path_)
    allimg_path = _path_ + '/train.traj'
    data_all__ = Trajectory(allimg_path)
    allimg_num = len(data_all__)
    x_old = pd.read_csv('./global_fps.csv').iloc[:, 1:]
    x_max = x_old.max(axis=0).values
    x_min = x_old.min(axis=0).values
    x_0_1 = np.array((x_old-x_min)/(x_max-x_min))
    x_data_select_np = np.transpose(x_0_1)
    x_data_select1 = pd.DataFrame(x_data_select_np, columns=[i for i in range(x_data_select_np[0].shape[0])])
    x_data_select1.to_csv('./global_fps0_1.csv')
    print('Run CUR Decomposition. ')
    select_img_index_sort = []
    print('Shape          fps_matrix: {}'.format(x_data_select1.shape))
    for iter in range(allimg_num):
        if iter == 0:
            last_selected_fps_index = calc_matrix_scores(matrix_df=x_data_select1)
            original_column_selected = np.array(x_data_select1)[:, last_selected_fps_index]
            column_name = x_data_select1.columns[last_selected_fps_index]
            select_img_index_sort.append(column_name)
            del x_data_select1[column_name]
            print('Iter: {}        |        Shape  fps_matrix: {}'.format(iter, x_data_select1.shape))
        else:
            matrix_copy = x_data_select1[:]
            # Orthogonalization.
            func_subtract_proj = col_proj_sub(matrix_df=matrix_copy, 
                                              orig_data_col=original_column_selected)
            # Cal the「importance score」of the「orthogonalized matrix」.And get index.
            last_selected_fps_index = calc_matrix_scores(func_subtract_proj)
            # The fps_vector of this time is obtained in the original matrix.
            original_column_selected = np.array(x_data_select1)[:, last_selected_fps_index]
            column_name = x_data_select1.columns[last_selected_fps_index]
            select_img_index_sort.append(column_name)
            del x_data_select1[column_name]
            print('Iter: {}        |        Shape  fps_matrix: {}'.format(iter, x_data_select1.shape))
    index_sort_df = pd.DataFrame(np.array(select_img_index_sort))
    index_sort_df.to_csv('./index_sorted.csv', index=None, header=['Index_sorted'])
    print('Finish CUR_Decomposition')
    return select_img_index_sort
            
            
def get_img():
    print('Program Initiated.')
    code_time_s = time.perf_counter()
    if not os.path.exists('./index_sorted.csv'):
        cur_decomposition(_path_=path_data)
    img_all = Trajectory(path_data+'/train.traj')
    index_sorted = pd.read_csv('/Users/renzhe/Desktop/index_sorted.csv')['Index_sorted'].tolist()
    for num_ in want_img_list:
        img_s = [img_all[i_] for i_ in index_sorted[:num_]]
        ase.io.write(images=img_s, filename='{}img_byCUR.traj'.format(num_))
        print('Select {} img. '.format(num_))
    code_time_e = time.perf_counter()
    print(
        'Program execution completed successfully. Code-Time is {} Sec.'.format(np.round(code_time_e-code_time_s, 3)))
        
get_img()        
        