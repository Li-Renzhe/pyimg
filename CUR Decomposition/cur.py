# -*- coding: utf-8 -*-
"""
---------------------------------------------------
[File-Information]
    -. Created on 2024/01/02
    -. Updated on 2024/01/11
    -. Developer: Akksay singh, Renzhe Li, Lei Li*
    -. Email: akksay@utexas.edu, 1135116956@qq.com
    -. Development Team: SUSTech Materials Department, Akksay singh, Renzhe Li, Lei Li*
---------------------------------------------------
[Description]
--CUR decomposition technology based on global descriptors performs structural screening on the data set.
    -. This code has a good interface with Pyamff (https://gitlab.com/pyamff/pyamff);
    -. 'element' is the type of element in your system;    
    -. 'num_img_' is the maximum number of structures you want to select;
    -. Set 'imgnuml_' = [n1, n2, n3,...] to run the program in batches. Note that 'ni' <= 'num_img_'.
    -. 'path_': Make sure the 'train.traj' and 'fingerprints files' are in this directory.
"""

# 0.Parameter setting
element = 'Ge'
num_img_ = 3558
imgnuml_ = [3557, 3013, 2529, 2083, 1764, 1464, 1264, 1049, 877, 722, 639, 556, 432, 392]
path_ = '/work/mse-minzw/lrz/aks/ge7506'

# 1.Import all tools.
print("Program Initiated.")
import io
import os
import re
import sys
import ase
import time
import torch
import pickle
import random
import sklearn
import numpy as np
import pandas as pd
from scipy import linalg
from ase.io.trajectory import Trajectory

# 2.Global Fingerprints for one image.
def cal_global_fingerprint(fp_path, el_=element):
    with open(fp_path, 'rb') as f:
        fp_all = pickle.load(f)
        fp_i = fp_all.allElement_fps[el_]
        time1_s = time.perf_counter()
        g_f = torch.sum(fp_i, axis=0) / len(fp_i)
    time1_e = time.perf_counter()
    time1 = time1_e - time1_s
    return g_f, time1

# 3.Global Fingerprints for all images.
def g_f_traj(path_all):
    time_all = 0
    gf_bank = []
    img_path = path_all + '/train.traj'
    img_all = Trajectory(img_path)
    img_num = len(img_all)
    for i in range(img_num):
        i_path = path_all + '/fingerprints/fps_{}.pckl'.format(i)
        res_i,t_ = cal_global_fingerprint(fp_path=i_path, el_='Ge')
        time_all += t_
        res_i_np = np.array(res_i)
        gf_bank.append(res_i_np)
        if i % 100 == 0:
            print('Finish Cal-Global-Fingerprints {}'.format(i))
    df_gf = pd.DataFrame(gf_bank)
    df_gf.to_csv('./global_fps.csv', encoding='utf-8', index=True)
    print('Calculate Global_Fingerprints. The Run-Time:', time_all)
    
# 4.SVD --> score.
def score_from_kth_sing_vec(k,V,cnoa):
    partial_score = 0.0
    for i in range(k): #again, first singular vector is 0th index
        score = (V[i][cnoa]) ** 2
        partial_score += score 
    return partial_score

# 5.INPUT matrix to this function has the column deleted and the original column.
def col_proj_sub(matrix,orig_data_col):
    selected_col = orig_data_col  
    num_cols = np.shape(matrix)[1]
    norm_selected_col = np.linalg.norm(orig_data_col) 
    for col in range(num_cols):
        dot_prod = np.dot(matrix[:,col],selected_col)
        proj = dot_prod / (norm_selected_col ** 2)
        matrix[:,col] -= selected_col * proj
    return matrix

# 6.Function to compute matrix scores of each column of original data matrix.
def calc_matrix_scores(matrix):
    U, s, Vh = linalg.svd(matrix)
    num_cols = np.shape(matrix)[1]
    scores_col = [score_from_kth_sing_vec(1,Vh,col) for col in range(num_cols)]
    return scores_col

# 7.Find index.
def find_loc_col(matrix,search):
    location = np.where((matrix[:, :]==search.reshape(-1, 1)).all(axis=0))[0][0]
    return location

# 8. Run-CUR_Decomposition.
def cur_decomposition(num_tot_img_select, _path_):
    if not os.path.exists('./global_fps.csv'):
        g_f_traj(path_all=_path_)       
    allimg_path = _path_ + '/train.traj'
    data_all__ = Trajectory(allimg_path)
    allimg_num = len(data_all__)   
    x_old = pd.read_csv('./global_fps.csv').iloc[:,1:]
    x_max = x_old.max(axis=0).values
    x_min = x_old.min(axis=0).values
    x_data_select = np.transpose(np.array((x_old-x_min)/(x_max-x_min)))  
    print ('Run CUR Decomposition. ')
    fp_paras_index_select = []
    num_selected = 0
    global_ranking = np.zeros(allimg_num) #Ge global fps
    np_fps_matrix = x_data_select
    original_fps_matrix = np.matrix.copy(x_data_select)
    for iter in range(num_tot_img_select):
        print ('Iter: {}       |       Shape np_fps_matrix: {}'.format(iter,np.shape(np_fps_matrix)))
        if num_selected == 0:
            #for original data matrix, compute scores for each column
            scores_col = calc_matrix_scores(np_fps_matrix)
            #now from the scores, get the column index of largest score
            last_selected_fps_index = scores_col.index(max(scores_col))#get index of fps selected 
            #now we need to record the column index we selected w.r.t original data matrix, 
            #and keep the column to compute projections of other unselected columns to it
            num_selected += 1 # increase the number of columns selected 
            original_column_selected = np_fps_matrix[:,last_selected_fps_index] #get the original column from matrix
            global_ranking[last_selected_fps_index] = num_selected # update location of selected col w.r.t orginal matrix
            #delete column of data previously selected also (very bad coding)
            np_fps_matrix = np.delete(np_fps_matrix,last_selected_fps_index,1)
            # now to compute the next column, we would take the shortened data matrix; subtract proj. of selected col w.r.t others
            # then calculate matrix scores of that matrix (with subtracted projections). then repeat
        else:
            func_subtract_proj = col_proj_sub(np.copy(np_fps_matrix),original_column_selected)
            #for the data matrix at the moment, compute matrix scores
            scores_col = calc_matrix_scores(func_subtract_proj) 
            last_selected_fps_index = scores_col.index(max(scores_col)) # get index of fps selected 
            num_selected += 1
            original_column_selected = np_fps_matrix[:,last_selected_fps_index]
            global_pos = find_loc_col(original_fps_matrix,original_column_selected) # array of 1 number
            global_ranking[global_pos] = num_selected
            # delete column of data previously selected also (very bad coding)
            np_fps_matrix = np.delete(np_fps_matrix,last_selected_fps_index,1)
    print ('Columns Selected: ',global_ranking)
    np.savetxt('image_index.txt', global_ranking) # Saving the array in a text file
    return None
    
# 9. select structures.
def select_structures(path_old,img_num_l,want_img):
    code_time_s = time.perf_counter()
    cur_decomposition(num_tot_img_select=want_img,_path_=path_old)
    path_ = path_old + '/train.traj'
    data_all = Trajectory(path_)
    cur_data = np.loadtxt('./image_index.txt')
    cur_data = list(cur_data.astype(int))
    for num_ in img_num_l:
        index_list_ = [list(cur_data).index(i) for i in range(1,num_+1)]
        img_cur = [data_all[i_] for i_ in index_list_]
        ase.io.write(filename='./cur_img{}.traj'.format(num_), images=img_cur)
        print('Finish {}'.format(num_))
    code_time_e = time.perf_counter()
    print('Program execution completed successfully. Code-Time is {} Sec.'
          .format(np.round(code_time_e-code_time_s, 2)))
    print('----------------^*^-D-O-N-E-^*^---------------')
    return None

# 10. Run my code. And hope you can get some good results.
select_structures(path_old=path_,
                  want_img=num_img_,
                  img_num_l=imgnuml_)      