w_n = 3557
img_path_='/work/mse-lirz/work/ge7506/benchmark/train.traj'
matrix_path_='/work/mse-lirz/work/fps_img/soap_fps/dis_matr/dis_matrix.csv'


import ase
import time
import torch
import numpy as np
import pandas as pd
from ase.io.trajectory import Trajectory


cpu_threads = 1
torch.set_num_threads(cpu_threads)


start_time = time.perf_counter()


# get structure symbol
def get_symbols(image_path):
    img = Trajectory(image_path)
    image = img[0]
    symbols_all = image.get_chemical_symbols()
    s = symbols_all[0]
    return s


# fps-core
def get_new_ma(matrix_img, compare_index):
    ma_i = matrix_img.iloc[compare_index, :]
    core_time_s = time.perf_counter()
    max_i = np.argmax(np.min(ma_i, axis=0))
    col_ = matrix_img.columns
    _col = col_[max_i]
    matrix_new = matrix_img.drop('{}'.format(_col), axis=1)
    _col = int(_col)
    core_time_e = time.perf_counter()
    core_time = core_time_e - core_time_s
    return _col, matrix_new, core_time


# fps-run
def farthest_point_sampling(want_p_num,
                            img_path,
                            matrix_path):
    print('DONE-「FPS」')
    time_fps = 0
    # initial parameters
    index_ = []
    img_all = Trajectory(img_path)
    img_num = len(img_all)
    dis_matrix = pd.read_csv(matrix_path)
    index_all = [i for i in range(len(img_all))]
    max_0 = 0
    di_0 = 1000

    # first step
    first_time_s = time.perf_counter()
    for i in range(img_num):
        dis_i_all = dis_matrix.iloc[i, :]
        di = (np.sum(dis_i_all) - dis_matrix.iloc[i, i]) / (img_num - 1)
        if di < di_0:
            di_0 = di
            max_0 = i
    first_time_e = time.perf_counter()
    first_time = first_time_e - first_time_s
    time_fps += first_time
    index_.append(max_0)

    # second step
    dis_matrix = dis_matrix.drop('{}'.format(max_0), axis=1)
    second_time_s = time.perf_counter()
    mat0 = dis_matrix.iloc[max_0, :]
    mat01 = np.max(mat0)
    ind = np.where(mat0 == mat01)[0][0]
    max_1 = int(dis_matrix.columns[ind])
    second_time_e = time.perf_counter()
    second_time = second_time_e - second_time_s
    time_fps += second_time
    index_.append(max_1)
    dis_matrix = dis_matrix.drop('{}'.format(max_1), axis=1)

    # 3,4，...,S step-FPS
    go_on = True
    while go_on:
        img_jieshou_i, matrix_new_, core_time_ = get_new_ma(matrix_img=dis_matrix, compare_index=index_)
        time_fps += core_time_
        index_.append(img_jieshou_i)
        dis_matrix = matrix_new_
        if len(index_) % 1 == 0:
            print('Progress: {}/{}        FPS-RUN-{} sec.        ALL-RUN-{} sec.'.format(len(index_), want_p_num,
                                                                                         np.round(core_time_, 3),
                                                                                         np.round(time_fps, 3)))
        if len(index_) == want_p_num:
            go_on = False

    # to .traj file
    accept_img = []
    for ai in index_:
        accept_img.append(img_all[ai])
    ase.io.write(images=accept_img, filename='train.traj')
    q_set = set(index_)
    qq_set = set(index_all)
    res = qq_set - q_set
    res_list = list(res)
    test_img = []
    for nai in res_list:
        test_img.append(img_all[nai])
    ase.io.write(images=test_img, filename='test.traj')
    print('DONE-「FPS」')
    return time_fps


fps_t = farthest_point_sampling(want_p_num=w_n,
                                img_path=img_path_,
                                matrix_path=matrix_path_)


end_time = time.perf_counter()
duration = end_time - start_time
print(f"FPS time is: {fps_t} sec.")
print(f"Running time is: {duration} sec.")
