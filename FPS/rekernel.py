i_p = './train.traj'
import time
import numpy as np
import pandas as pd
from dscribe.descriptors import SOAP
from ase.io.trajectory import Trajectory
from dscribe.kernels import REMatchKernel
from sklearn.preprocessing import normalize

import torch
torch.set_num_threads(1)

print("Program Initiated.")
code_time_s = time.perf_counter()

n_m_ = 4
l_m_ = 3


# get structure symbol
def get_symbols(image_path):
    img = Trajectory(image_path)
    image = img[0]
    symbols_all = image.get_chemical_symbols()
    s = symbols_all[0]
    return s


def soap_get_similarity(r_c, n_m, l_m, symbol, img_path):
    time_code_s = time.perf_counter()
    print('Start-「soap_get_similarity」-Job')

    # define-soap
    soap_descriptor = SOAP(species=[symbol],
                           r_cut=r_c,
                           n_max=n_m,
                           l_max=l_m,
                           sigma=0.2,
                           periodic=True,
                           sparse=False)
    data_all = Trajectory(img_path)
    img_num = len(data_all)
    shape_ma = (img_num, img_num)
    similarity_matrix = np.zeros(shape_ma)

    # cal-soap
    cal_soap_s = time.perf_counter()
    soap_list = []
    for s in range(img_num):
        soap_s = soap_descriptor.create(data_all[s])
        soap_1 = normalize(soap_s)
        soap_list.append(soap_1)
    cal_soap_e = time.perf_counter()
    cal_soap_time = cal_soap_e - cal_soap_s
    print(cal_soap_time)
    # cal-similarity-matrix
    cal_sim_ma_s = time.perf_counter()
    re = REMatchKernel(metric='linear',
                       alpha=1,
                       threshold=1e-6)
    s_m_ij = re.create(soap_list)
    similarity_matrix = pd.DataFrame(s_m_ij)
    cal_sim_ma_e = time.perf_counter()
    cal_sim_ma_time = cal_sim_ma_e - cal_sim_ma_s
    print('DONE-「soap_get_similarity」-Job')

    # save --> csv
    sim_matrix = pd.DataFrame(similarity_matrix, columns=np.arange(img_num))
    sim_matrix.to_csv('sim_matrix.csv', index=False)

    # end
    print('DONE-「soap_get_similarity」-Job')
    time_code_e = time.perf_counter()
    time_code = time_code_e - time_code_s

    # print-some-information
    print(f"Cal-soap-time:                     {cal_soap_time} sec.")
    print(f"cal-similarity-matrix-time is:     {cal_sim_ma_time} sec.")
    print(f"All-code-time:                     {time_code} sec.")


def A_B_dis(sim_AB):
    d = np.sqrt(2 - 2 * sim_AB + 1e-15)
    return d


def distance_matrix(simi_matr_path, img_path):
    dis_time_s = time.perf_counter()
    img_all = Trajectory(img_path)
    img_num = len(img_all)
    shape_ma = (img_num, img_num)
    dis_matrix = np.zeros(shape_ma)
    simi_matr = pd.read_csv(simi_matr_path)
    for i in range(0, img_num):
        for j in range(i, img_num):
            simi_ij = simi_matr.iloc[i, j]
            dis_ij = A_B_dis(simi_ij)
            dis_matrix[i, j] = dis_ij
            dis_matrix[j, i] = dis_ij
    df = pd.DataFrame(dis_matrix, columns=np.arange(img_num))
    df.to_csv('dis_matrix.csv', index=False)
    dis_time_e = time.perf_counter()
    dis_time = dis_time_e - dis_time_s
    return dis_time


sy = get_symbols(image_path=i_p)
soap_get_similarity(r_c=5.7,
                    n_m=n_m_,
                    l_m=l_m_,
                    symbol=sy,
                    img_path=i_p)
dis_time_ = distance_matrix(simi_matr_path='./sim_matrix.csv',
                            img_path=i_p)
code_time_e = time.perf_counter()
print('Program execution completed successfully.Code-Time is {}'.format(code_time_e - code_time_s))
