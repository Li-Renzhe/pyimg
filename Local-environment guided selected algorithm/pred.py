# -*- coding: utf-8 -*-
"""
@------------------------------------------------------------------------------------------------------------@
    Created on 2023/10/31, Updated on 2023/10/31
    Developer: Renzhe Li
    Email: 1135116956@qq.com
    Development Team: SUSTech Materials Department, Renzhe Li, Chuan Zhou, Lei Li*
@------------------------------------------------------------------------------------------------------------@

@description:
1.This code is predicted using the force field file given by pyamff after it is trained. 
  Variable: the absolute path of xxx.traj, that is, pa
2.The real energy and predicted energy of e and f will be output in the form of .csv file. 
  And f will give the three directions xyz.
3.There is no drawing function. After getting the .csv file of 2, draw the picture yourself.
4.This is the fortran version. This file only needs to be in the same directory as the mlff.pyamff file.
"""

# Parameter setting
pa = '/work/mse-minzw/lrz/aks/pdh/benchmark/train.traj'


# Define function
print("Program Initiated.")
import ase
import time
import torch
import numpy as np
import pandas as pd
from ase.io import Trajectory
from pyamff.aseCalcF import aseCalcF


code_time_s = time.perf_counter()
my_model = aseCalcF()


def get_true_e_and_f(data_path):
    true_energy = []
    true_force = []
    data = Trajectory(data_path)
    i = 0
    for ge in data:
        true_energy.append(ge.get_potential_energy())
        true_force.append(ge.get_forces())
        i += 1
        if i % 10 == 0:
            print('Get the real energy and force.          |          {}/{}'.format(i, len(data)))
    return true_energy, true_force


def get_pred_e_and_f(data_path, model=my_model):
    pred_energy = []
    pred_force = []
    i = 0
    data = Trajectory(data_path)
    for ge in data:
        ge.set_calculator(model)
        pred_energy.append(ge.get_potential_energy())
        pred_force.append(ge.get_forces())
        i += 1
        if i % 10 == 0:
            print('Predict the energy and force.           |           {}/{}'.format(i, len(data)))
    return pred_energy, pred_force


def get_rmse_e_and_f(pred_energy, pred_force, true_energy, true_force):
    num = len(pred_energy)
    er_e_list = []
    error_energy = np.array(pred_energy) - np.array(true_energy)
    i = 0
    for e_e in error_energy:
        er_e_list.append(e_e / len(true_force[i]))
        i += 1
        if i % 10 == 0:
            print('Calculate the EnergyRMSE of the {}th image.'.format(i))
    rmse_e = np.sqrt(sum((np.array(er_e_list) ** 2)) / num)
    er_f_list = []
    error_force = np.array(pred_force) - np.array(true_force)
    i = 0
    for e_f in error_force:
        er_f_list.append(np.sqrt(sum(sum(e_f ** 2)) / (3 * len(e_f))))
        i += 1
        if i % 10 == 0:
            print('Calculate the ForceRMSE of the {}th image.'.format(i))
    rmse_f = np.sqrt(sum(np.array(er_f_list) ** 2) / num)
    return rmse_e, rmse_f


def write_e_and_f_to_file(true_energy, pred_energy, true_force, pred_force):
    num = len(true_force)
    true_energy_num = []
    pred_energy_num = []
    pred_force_num = []
    true_force_num = []
    for i in range(num):
        true_energy_num.append(true_energy[i] / len(true_force[i]))
        pred_energy_num.append(pred_energy[i] / len(true_force[i]))
    data_e = pd.DataFrame()
    data_e['true_energy'] = true_energy_num
    data_e['pred_energy'] = pred_energy_num
    data_e.to_excel('energy.xlsx', sheet_name='energy', index=True)

    data = pd.DataFrame()
    for f_ in true_force:
        data_f = pd.DataFrame()
        f_x = []
        f_y = []
        f_z = []
        for i in range(len(f_)):
            f_x.append(f_[i][0])
            f_y.append(f_[i][1])
            f_z.append(f_[i][2])
        data_f['true_force_x'] = f_x
        data_f['true_force_y'] = f_y
        data_f['true_force_z'] = f_z
        data = pd.concat([data, data_f], axis=0)
    data.to_excel('force_true.xlsx', sheet_name='force_true', index=True)

    data = pd.DataFrame()
    for f_ in pred_force:
        data_f = pd.DataFrame()
        f_x = []
        f_y = []
        f_z = []
        for i in range(len(f_)):
            f_x.append(f_[i][0])
            f_y.append(f_[i][1])
            f_z.append(f_[i][2])
        data_f['pred_force_x'] = f_x
        data_f['pred_force_y'] = f_y
        data_f['pred_force_z'] = f_z
        data = pd.concat([data, data_f], axis=0)
    data.to_excel('force_pred.xlsx', sheet_name='force_pred', index=True)


# Encapsulate all the above functions as main functions and run.
def run_calRMSE_img():
    true_energy, true_force = get_true_e_and_f(data_path=pa)
    print('Successfully achieved real energy and force.')
    pred_energy, pred_force = get_pred_e_and_f(data_path=pa, model=my_model)
    print('Successfully achieved predicted energy and force.')
    write_e_and_f_to_file(true_energy, pred_energy, true_force, pred_force)
    print('Successfully written and saved real and predicted energies and forces to file and saved as Excel sheet.')
    rmse_e, rmse_f = get_rmse_e_and_f(pred_energy=pred_energy, pred_force=pred_force,
                                      true_energy=true_energy, true_force=true_force)
    print('Successfully calculated EnergyRMSE and ForceRMSE of the test set.')
    print('PRINT-RMSE:')
    rmse_e = format(rmse_e, ".15f")
    rmse_f = format(rmse_f, ".15f")
    print('EnergyRMSE={},ForceRMSE={}'.format(rmse_e, rmse_f))
    code_time_e = time.perf_counter()
    print(
        'Program execution completed successfully.Code-Time is {} Sec.'.format(np.round(code_time_e-code_time_s, 2)))


run_calRMSE_img()