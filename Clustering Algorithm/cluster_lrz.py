# 1.Global Fingerprints for Ge-7506
import io
import os
import re
import ase
import time
import torch
import pickle
import random
import sklearn
import numpy as np
import pandas as pd
from ase.io.trajectory import Trajectory
from sklearn.cluster import AgglomerativeClustering

torch.set_num_threads(1)

# 1.Global Fingerprints for Ge-7506
def cal_global_fingerprint(fp_path, el_='Ge'):
    with open(fp_path, 'rb') as f:
        fp_all = pickle.load(f)
        fp_i = fp_all.allElement_fps[el_]
        time1_s = time.perf_counter()
        g_f = torch.sum(fp_i, axis=0) / len(fp_i)
    time1_e = time.perf_counter()
    time1 = time1_e - time1_s
    return g_f, time1

def g_f_traj(path_all):
    time_all = 0
    gf_bank = []
    img_path = path_all + '/train.traj'
    img_all = Trajectory(img_path)
    img_num = len(img_all)
    for i in range(img_num):
        i_path = path_all + '/fingerprints/fps_{}.pckl'.format(i)
        res_i,t_ = cal_global_fingerprint(fp_path=i_path, el_='Ge')
        time_all += t_
        res_i_np = np.array(res_i)
        gf_bank.append(res_i_np)
        if i % 500 == 0:
            print('Finish cal_g_f {}'.format(i))
    df_gf = pd.DataFrame(gf_bank)
    df_gf.to_csv('./global_fps.csv', encoding='utf-8', index=True)
    print('Calculate Global_Fingerprints. The Run-Time:', time_all)


# 2.Clustering algorithms(e.g. sklearns agglomerative clustering on top of the dissimilarity matrices)
def agglomerative_clustering(g_f_path,img_num_list,all_imgpath):
    img_all = Trajectory(all_imgpath)
    x_old = pd.read_csv(g_f_path).iloc[:,1:]
    time_1_s = time.perf_counter()
    x_max = x_old.max(axis=0).values
    x_min = x_old.min(axis=0).values
    x = (x_old-x_min)/(x_max-x_min)
    time_1_e = time.perf_counter()
    time_1_a = time_1_e - time_1_s
    print('0-1 Run-Time:',time_1_a)
    for img_num in img_num_list:
        time_i_s = time.perf_counter()
        img_cluster = []
        clustering = sklearn.cluster.AgglomerativeClustering(
                     memory=None,
                     linkage='average',
                     metric='euclidean',
                     n_clusters=img_num,
                     compute_full_tree='auto',).fit(x)
        res_clusters = clustering.labels_
        index_dict = {}
        for i, num in enumerate(res_clusters):
            if num in index_dict:
                index_dict[num].append(i)
            else:
                index_dict[num] = [i]
        sorted_dict = dict(sorted(index_dict.items()))
        for j in range(img_num):
            random_value = random.choice(sorted_dict[j])
            img_cluster.append(random_value)
        sel_img = [img_all[k] for k in img_cluster]
        time_i_e = time.perf_counter()
        time_i = time_i_e - time_i_s
        ase.io.write(filename='./{}cluster_img.traj'.format(img_num),images=sel_img)
        print('Finish {}-image.  Clustering_Run-Time:{}'.format(img_num,time_i))
    return None


img_num_l = [3557, 3013, 2529, 2083, 1764, 1464, 1264, 1049, 877, 722, 639, 556, 432, 392]
if not os.path.exists('./global_fps.csv'):
    g_f_traj(path_all='/work/mse-minzw/lrz/aks/ge7506')
agglomerative_clustering(img_num_list=img_num_l,
                         g_f_path='./global_fps.csv',
                         all_imgpath='/work/mse-minzw/lrz/aks/ge7506/train.traj')
